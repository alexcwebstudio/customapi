import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

interface TestData {
  id: number;
  name: string;
  isComplete: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  readonly configUrl = 'https://localhost:5001/api/todo';

  constructor(private http: HttpClient) { }

  data$: Observable<TestData[]>;
  newData$: Observable<any>;

  getData() {
    this.data$ = this.http.get<TestData[]>(this.configUrl);
  }

  postData() {
    const postData: TestData = {
      id: 79,
      name: 'My New Post',
      isComplete: true
    };

    this.newData$ = this.http.post(this.configUrl, postData);
  }

}
